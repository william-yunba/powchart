var gulp = require('gulp'),
    header = require('gulp-header'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    clean = require('gulp-clean');

var pkg = require('./package.json');
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');

var path = require('path');
var PATH = {
    src: path.join(__dirname, '/src'),
    dist: path.join(__dirname, '/dist')
};

gulp.task('js', function () {
    return gulp.src(PATH.src + '/*.js')
      .pipe(sourcemaps.init())
      .pipe(uglify())
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(header(banner, { pkg : pkg } ))
      .pipe(gulp.dest(PATH.dist));
});

gulp.task('css', function () {
    return gulp.src(PATH.src + '/*.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({
          suffix: '.min'
        }))
        .pipe(header(banner, { pkg : pkg } ))
        .pipe(gulp.dest(PATH.dist));
});

gulp.task('clean', function () {
    return gulp.src(PATH.dist +'/*', { read: false })
        .pipe(clean({ force: true }));
});

gulp.task('build', ['clean', 'css', 'js']);

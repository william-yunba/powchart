(function (root, factory) {
    if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        var Highcharts = require('highcharts');
        module.exports = factory(root,
            require('jquery'),
            require('moment'),
            Highcharts,
            require('highcharts/highcharts-more'),
            require('highcharts/module/solid-gauge')(Highcharts)
        );
    } else {
        // Browser globals (root is window)
        root.Powchart = factory(root, root.$, root.moment, root.Highcharts);
    }
}(this, function (root, $, moment, Highcharts) {
    // https://github.com/blueimp/JavaScript-Templates
    /*eslint-disable */
    !function(e) {
        "use strict";
        var n = function(e, t) {
            var r = /[^\w\-\.:]/.test(e) ? new Function(n.arg + ",tmpl", "var _e=tmpl.encode" + n.helper + ",_s='" + e.replace(n.regexp, n.func) + "';return _s;") : n.cache[e] = n.cache[e] || n(n.load(e));
            return t ? r(t, n) : function(e) {
                return r(e, n)
            }
        };
        n.cache = {}, n.load = function(e) {
            return document.getElementById(e).innerHTML
        }, n.regexp = /([\s'\\])(?!(?:[^{]|\{(?!%))*%\})|(?:\{%(=|#)([\s\S]+?)%\})|(\{%)|(%\})/g, n.func = function(e, n, t, r, c, u) {
            return n ? {
                "\n": "\\n",
                "\r": "\\r",
                "  ": "\\t",
                " ": " "
            }[n] || "\\" + n : t ? "=" === t ? "'+_e(" + r + ")+'" : "'+(" + r + "==null?'':" + r + ")+'" : c ? "';" : u ? "_s+='" : void 0
        }, n.encReg = /[<>&"'\x00]/g, n.encMap = {
            "<": "&lt;",
            ">": "&gt;",
            "&": "&amp;",
            '"': "&quot;",
            "'": "&#39;"
        }, n.encode = function(e) {
            return (null == e ? "" : "" + e).replace(n.encReg, function(e) {
                return n.encMap[e] || ""
            })
        }, n.arg = "o", n.helper = ",print=function(s,e){_s+=e?(s==null?'':s):_e(s);},include=function(s,d){_s+=tmpl(s,d);}", "function" == typeof define && define.amd ? define(function() {
            return n
        }) : "object" == typeof module && module.exports ? module.exports = n : e.tmpl = n
    }(root);
    var tmpl = root.tmpl;
    /*eslint-enable */

    // 指标基类
    function Indicator(opts) {
        var params = opts.params || {};
        this.chart = opts.chart;
        this.info = opts.info || {};
        this.useSubChart = opts.useSubChart || false;
        this.params = params;
        this.url = this.info.url.replace(/:([^\/]+)/g, function (mstr, propertyName) {
            return params[propertyName];
        });
        this.id = this.genId();
        this.name = this.genName();
        this.setFilters(opts.filters);
    }
    Indicator._ids = {};
    Indicator.prototype = {
        toJSON: function () {
            return {
                params: this.params,
                filters: this.filters,
                constructorName: this.name
            };
        },
        genId: function () {
            var id = '_indicator' + Math.random().toString().substr(2);
            if (Indicator._ids[id]) {
                return this.genId();
            }
            return id;
        },
        genName: function () {
            var name = '';
            var params = this.params;
            var filters = this.filters;
            var k;
            $.each(params, function (i, v) {
                name += v + '-';
            });
            for (k in filters) {
                if (filters.hasOwnProperty(k)
                    && filters[k].enabled && filters[k].values.length > 0) {
                    name += ('[' + filters[k].values.toString() + ']-');
                }
            }
            name += this.info.name;
            return name;
        },
        // 首次加载series和subseries
        init: function (opts, cb) {
            this.load({
                start: opts.start,
                end: opts.end,
                unit: opts.unit
            }, $.proxy(function (err, data) {
                var seriesOpts = {
                    name: this.name,
                    yAxis: this.info.yAxisId,
                    data: data
                };
                if (this.info.yAxisId === 'percentage') {
                    seriesOpts.tooltip = {
                        valueSuffix: '%'
                    };
                }
                this.series = this.chart.addSeries(seriesOpts);
                this.addLabel();
                if (this.useSubChart) {
                    this.addSubChart();
                }
                cb(null);
            }, this));
        },

        // options.refresh
        update: function (opts) {
            this.load({
                start: opts.start,
                end: opts.end,
                unit: opts.unit
            }, $.proxy(function (err, data) {
                var series = this.series;
                var cb = opts.callback;
                if (opts.refresh) {
                    series.setData(data);
                } else {
                    data.forEach(function (point) {
                        series.addPoint(point, true, true);
                    });
                }
                if (this.useSubChart) {
                    this.updateSubChart();
                }
                if (cb && typeof cb === 'function') {
                    cb(null, this);
                }
            }, this));
        },

        setFilters: function (filters) {
            var filtersParam = {};
            filters = filters || {};
            $.each(filters, function (i, v) {
                if (v.enabled && v.values.length > 0) {
                    filtersParam[i] = v.values;
                }
            });
            this.filters = filters;
            this.name = this.genName();
            $('.chart-indicator-filter-form option[value="' + this.id + '"]').text(this.name);
            if (this.$label) {
                this.$label.find('span').text(this.name);
            }
            this.filtersParam = JSON.stringify(filtersParam);
        },

        destroy: function () {
            if (this.useSubChart) {
                this.subChart.destroy();
                this.$subChart.remove();
            }
            this.$label.remove();
            this.series.destroy();
        },

        load: function (opts, cb) {
            $.ajax({
                method: 'get',
                url: this.url,
                data: {
                    start: opts.start,
                    end: opts.end,
                    unit: opts.unit,
                    filters: this.filtersParam === '{}' ? undefined : this.filtersParam
                },
                success: function (result) {
                    cb(null, result);
                },
                error: function (xhr, textStatus, err) {
                    cb(err);
                }
            });
        },
        toggle: function () {
            var $label = this.$label;
            if ($label.hasClass('disabled')) {
                $label.removeClass('disabled').css({
                    'background-color': this.series.color
                });
                this.series.show();
                if (this.useSubChart) {
                    this.subChart.series[0].show();
                }
            } else {
                $label.addClass('disabled').css({
                    'background-color': '#ccc'
                });
                this.series.hide();
                if (this.useSubChart) {
                    this.subChart.series[0].hide();
                }
            }
        },
        onMouseOver: function () {
            this.series.onMouseOver();
        },
        onMouseOut: function () {
            this.series.onMouseOut();
        },
        labelTmpl: tmpl([
            /*eslint-disable */
            '{% var id = o.id, color = o.series.color, name = o.name, filterStr = o.filterStr;%}',
            '<a data-id="{%= id %}" class="label" style="background-color:{%= color %}">',
                '<span>{%= name %}</span>',
                '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
            /*eslint-enable */
        ].join('')),
        addLabel: function () {
            this.$label = $(this.labelTmpl(this));
            $(this.chart.container).parent().siblings('.chart-legend').prepend(this.$label);
        },
        updateLabel: function () {
            if (this.$label) {
                this.$label.find('span').text(this.name);
            }
        },
        addSubChart: function () {
            var data = this.series.data;
            var value = data[data.length - 1].y;
            this.$subChart = $('<div class="subchart-item"></div>');
            $(this.chart.container).parent().siblings('.chart-subchart').prepend(this.$subChart);
            var gaugeOptions = {
                chart: {
                    type: 'solidgauge',
                    renderTo: this.$subChart[0],
                    backgroundColor: 'rgba(0,0,0,0)'
                },
                title: null,
                pane: {
                    center: ['50%', '50%'],
                    size: '100%',
                    startAngle: 0,
                    endAngle: 360,
                    background: {
                        backgroundColor: '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        borderWidth: 0,
                        shape: 'arc'
                    }
                },
                tooltip: {
                    enabled: false
                },
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    stops: [
                        [0.2, '#55BF3B'],
                        [0.5, '#DDDF0D'],
                        [0.9, '#DF5353']
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickPixelInterval: 400,
                    tickWidth: 0,
                    title: {
                        y: 0,
                        text: null
                    },
                    labels: {
                        y: 16
                    },
                    min: 0,
                    max: 100
                },
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: -10,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    data: [value],
                    dataLabels: {
                        format: '<div style="text-align:center;" title="'
                            + this.name + '">{y}%</div>'
                    }
                }]
            };
            this.subChart = new Highcharts.Chart(gaugeOptions);
        },
        updateSubChart: function () {
            var data = this.series.data;
            var value = data[data.length - 1].y;
            this.subChart.series[0].points[0].update(value);
        }
    };

    function Powchart(opts) {
        var now = moment();
        opts = opts || {};
        this.el = opts.el;
        this.end = moment(opts.end ? opts.end : now);
        this.start = opts.start ? moment(opts.start) : moment(now - 1800000);
        this.unit = opts.unit ? opts.unit : Powchart.UNITS[0].value;
        this.initIndicators = opts.initIndicators || [];
        this.availableIndicators = opts.availableIndicators || [];
        this.useSubChart = opts.useSubChart || false;
        this.yaxises = {};
        this.indicators = [];
        this.pause = false;
        this.interval = undefined;
        this.init();
    }

    Powchart.prototype = {
        constructor: Powchart,
        $: function (selector) {
            return this.$el.find(selector);
        },
        isIE8: Boolean(navigator.userAgent.match(/msie 8/i)),
        init: function () {
            $($.proxy(function () {
                var $el = $(this.el);
                $el.html(this.chartTmpl({ units: Powchart.UNITS }));
                var $indicatorForm = $el.find('.chart-indicator-form');
                $indicatorForm.html(this.indicatorFormTmpl());
                var indicatorFormWidth = $indicatorForm.outerWidth();
                $indicatorForm.data('width', indicatorFormWidth);
                $indicatorForm.css({
                    right: -indicatorFormWidth,
                    display: 'block'
                });

                var $indicatorFilterForm = $el.find('.chart-indicator-filter-form');
                $indicatorFilterForm.html(this.indicatorFilterFormTmpl());
                $indicatorFilterForm.data('width', indicatorFormWidth);
                $indicatorFilterForm.css({
                    right: -indicatorFormWidth,
                    display: 'block',
                    width: indicatorFormWidth + 'px'
                });

                var $unit = $el.find('select[name="unit"]');
                $unit.find('[value="' + this.unit + '"]').prop('selected', true);
                this.setInterval();

                // chart
                this.chart = new Highcharts.Chart({
                    title: {
                        text: ''
                    },
                    chart: {
                        renderTo: $el.find('.chart-main')[0],
                        type: 'line'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    credits: {
                        enabled: false
                    },
                    series: [],
                    yAxis: [],
                    tooltip: {
                        formatter: function (tooltip) {
                            var format = tooltip.chart.powchart.getTooltipDateFormat();
                            var points = this.points, point, series;
                            var html = Highcharts.dateFormat(format, this.x) + '<br />';
                            for (var i = 0; i < points.length; i++) {
                                point = points[i];
                                series = point.series;
                                html += (series.name
                                    + ' : '
                                    + point.y
                                    + (series.tooltipOptions.valueSuffix || '')
                                    + '<br />');
                            }
                            return html;
                        },
                        shared: true
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                enabled: false
                            }
                        }
                    }
                });

                this.chart.powchart = this;
                this.$el = $el;
                this.$indicatorForm = $indicatorForm;
                this.$indicatorFilterForm = $indicatorFilterForm;
                this.$unit = $unit;
                this.initDateRangePicker();
                this.delegateEvents();

                var indicators = this.initIndicators;
                var doAddIndicator = $.proxy(this.doAddIndicator, this);
                $.each(indicators, function (index, value) {
                    doAddIndicator(value);
                });
            }, this));
        },

        getTooltipDateFormat: function () {
            var unit = this.unit;
            var format;
            switch (unit) {
            case '10s':
                format = '%Y-%m-%d %H:%M:%S';
                break;
            case '1m':
            case '5m':
            case '15m':
                format = '%Y-%m-%d %H:%M';
                break;
            case '1h':
            case '4h':
                format = '%Y-%m-%d %H';
                break;
            case '1d':
            case '7d':
            case '30d':
            default:
                format = '%Y-%m-%d';
                break;
            }
            return format;
        },

        setInterval: function () {
            if (this.interval) {
                root.clearInterval(this.interval);
            }
            var UNITS = Powchart.UNITS;
            var unit = this.unit, milliseconds;
            $.each(UNITS, function (k, v) {
                if (v.value === unit) {
                    milliseconds = v.milliseconds;
                    return;
                }
            });
            if (!milliseconds) {
                throw new Error('unit not available');
            }
            this.interval = root.setInterval($.proxy(this.update, this), milliseconds);
        },

        clearInterval: function () {
            root.clearInterval(this.interval);
        },

        update: function () {
            var now, unit, $daterange, daterange, format,
                updateStart, updateEnd, options;
            if (this.pause) {
                return;
            }
            now = moment();
            updateStart = this.end.valueOf();
            updateEnd = now.valueOf();
            unit = this.unit;
            options = {
                start: updateStart,
                end: updateEnd,
                unit: unit
            };

            this.indicators.forEach(function (indicator) {
                indicator.update(options);
            });

            this.start = this.start.add(now - this.end, 'milliseconds');
            this.end = now;
            $daterange = this.$daterange;
            daterange = $daterange.data('daterangepicker');
            format = daterange.locale.format;
            $daterange.val(this.start.format(format) + ' - ' + this.end.format(format));
        },

        initDateRangePicker: function () {
            var $daterange = this.$('.date-range');
            this.$daterange = $daterange;
            var start;
            function getRange(label) {
                switch (label) {
                case '30分钟':
                    start = moment().subtract(30, 'minutes');
                    break;
                case '1小时':
                    start = moment().subtract(1, 'hours');
                    break;
                case '4小时':
                    start = moment().subtract(4, 'hours');
                    break;
                case '1天':
                    start = moment().subtract(1, 'days');
                    break;
                case '7天':
                    start = moment().subtract(7, 'days');
                    break;
                case '30天':
                    start = moment().subtract(30, 'days');
                    break;
                default:
                    start = moment().subtract(1, 'days');
                    break;
                }
                return [start, moment()];
            }

            // override
            root.daterangepicker.prototype.originHoverRange =
                root.daterangepicker.prototype.hoverRange;
            root.daterangepicker.prototype.hoverRange = function (e) {
                var label, range, rangeStart, rangeEnd;
                // ignore mouse movements while an above-calendar text input has focus
                if (this.container.find('input[name=daterangepicker_start]').is(':focus')
                    || this.container.find('input[name=daterangepicker_end]').is(':focus')
                ) {
                    return;
                }

                label = e.target.innerHTML;
                if (label === this.locale.customRangeLabel) {
                    this.updateView();
                } else {
                    range = getRange(label);
                    rangeStart = range[0];
                    rangeEnd = range[1];
                    this.container.find('input[name=daterangepicker_start]')
                        .val(rangeStart.format(this.locale.format));
                    this.container.find('input[name=daterangepicker_end]')
                        .val(rangeEnd.format(this.locale.format));
                }
            };
            // override
            root.daterangepicker.prototype.originClickRange =
                root.daterangepicker.prototype.clickRange;
            root.daterangepicker.prototype.clickRange = function (e) {
                var label = e.target.innerHTML;
                var range;
                this.chosenLabel = label;
                if (label === this.locale.customRangeLabel) {
                    this.showCalendars();
                } else {
                    range = getRange(label);
                    this.startDate = range[0];
                    this.endDate = range[1];

                    if (!this.timePicker) {
                        this.startDate.startOf('day');
                        this.endDate.endOf('day');
                    }

                    if (!this.alwaysShowCalendars) {
                        this.hideCalendars();
                    }
                    this.clickApply();
                }
            };
                // time range
            $daterange.daterangepicker({
                timePicker: true,
                timePicker24Hour: true,
                timePickerSeconds: true,
                autoApply: true,
                linkedCalendars: false,
                startDate: this.start,
                endDate: this.end,
                ranges: {
                    '30分钟': '',
                    '1小时': '',
                    '4小时': '',
                    '1天': '',
                    '7天': '',
                    '30天': ''
                },
                locale: {
                    format: 'YYYY/MM/DD HH:mm:ss',
                    separator: ' - ',
                    applyLabel: '确定',
                    cancelLabel: '取消',
                    weekLabel: 'W',
                    customRangeLabel: '自定义',
                    daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'],
                    firstDay: moment.localeData().firstDayOfWeek()
                }
            }, $.proxy(this.selectDateRange, this))
                .data('daterangepicker')
                .container
                .addClass('powchart-daterange ' + this.isIE8 ? 'ie8' : '');

            this.disableUnits(this.start, this.end);
        },
        selectDateRange: function (start, end) {
            this.start = start;
            this.end = end;
            this.disableUnits(start, end);
            this.setInterval();
            this.refershIndacators();
        },
        selectUnit: function (e) {
            var unit = $(e.currentTarget).val();
            if (!unit) {
                return;
            }
            this.unit = unit;
            this.setInterval();
            this.refershIndacators();
        },

        disableUnits: function (start, end) {
            var $unit, unitsOfRange, originUnit, diff;
            diff = end - start;
            $.each(Powchart.UNITS_OF_RANGE, function (i, v) {
                if (v.min < diff && diff <= v.max) {
                    unitsOfRange = v.units;
                    return;
                }
            });
            $unit = this.$unit;
            $unit.find('option').prop('disabled', true);
            $.each(unitsOfRange, function (i, v) {
                $unit.find('[value="' + v + '"]').prop('disabled', false);
            });

            originUnit = this.unit;
            if ($.inArray(originUnit, unitsOfRange) === -1) {
                // 原来的unit不可用
                // 使用默认的
                originUnit = unitsOfRange[0];
                this.unit = originUnit;
                this.$unit.find('[value="' + originUnit + '"]').prop('selected', true);
            }
        },

        refershIndacators: function () {
            var options = {
                start: this.start.valueOf(),
                end: this.end.valueOf(),
                unit: this.unit,
                refresh: true
            };
            this.indicators.forEach(function (indicator) {
                indicator.update(options);
            });
        },

        delegateEvents: function () {
            var $el = this.$el;
            var $indicatorForm = this.$indicatorForm;
            var $indicatorFilterForm = this.$indicatorFilterForm;

            var toggleForm = this.toggleForm;
            $el.delegate('.toggle-indicator-form', 'click',
                $.proxy(toggleForm, this, $indicatorForm));
            $el.delegate('select[name="type"]', 'change',
                $.proxy(this.selectIndicator, this));
            $el.delegate('.chart-indicator-form form', 'submit',
                $.proxy(this.addIndicator, this));
            $el.delegate('.remove-chart', 'click',
                $.proxy(this.destroy, this));
            $el.delegate('select[name="filterIndicator"]', 'change',
                $.proxy(this.showIndicatorDimensions, this));
            $el.delegate('select.dimension-switch', 'change',
                $.proxy(this.toggleDimension, this));
            $el.delegate('.toggle-indicator-filter-form', 'click',
                $.proxy(toggleForm, this, $indicatorFilterForm));
            $el.delegate('.chart-indicator-filter-form form', 'submit',
                $.proxy(this.setIndicatorFilter, this));
            $el.delegate('.chart-legend .glyphicon-remove', 'click',
                $.proxy(this.removeIndicator, this));
            $el.delegate('.chart-legend a', 'click',
                $.proxy(this.toggleIndicator, this));
            $el.delegate('.chart-legend a', 'mouseover',
                $.proxy(this.mouseOverIndicator, this));
            $el.delegate('.chart-legend a', 'mouseout',
                $.proxy(this.mouseOutIndicator, this));
            $el.delegate('select[name="unit"]', 'change',
                $.proxy(this.selectUnit, this));
        },
        undelegateEvents: function () {

        },
        selectIndicator: function (event) {
            var val = $(event.currentTarget).val();
            if (val !== '') {
                this.$indicatorForm.find('.chart-indicator-params > ._for-' + val)
                    .addClass('active')
                    .siblings()
                    .removeClass('active');
            }
        },
        chartTmpl: tmpl([
            /*eslint-disable */
            '<div class="powchart">',
                '<div class="panel panel-default">',
                    '<div class="panel-heading">',
                        '<div class="btn-group">',
                            '<input type="text" class="form-control date-range">',
                        '</div>',
                        '<div class="btn-group">',
                            '<select class="form-control" name="unit">',
                                '{% var units = o.units; %}',
                                '{% for(var i=0, length = units.length ; i < length; i++) { %}',
                                    '<option value="{%= units[i].value%}" {%= (i==0 ? "selected" : "")%} >{%= units[i].title %}</option>',
                                '{% } %}',
                            '</select>',
                        '</div>',
                        '<div class="pull-right">',
                            '<button title="指标过滤" type="button" class="btn btn-default toggle-indicator-filter-form"><i class="glyphicon glyphicon-filter"></i></button>',
                            '<button title="添加指标" type="button" class="btn btn-default toggle-indicator-form"><i class="glyphicon glyphicon-plus"></i></button>',
                            '<button title="删除图表" type="button" class="btn btn-default remove-chart"><i class="glyphicon glyphicon-remove"></i></button>',
                        '</div>',
                    '</div>',
                    '<div class="panel-body chart-main"></div>',
                    '<div class="chart-subchart"></div>',
                    '<div class="chart-legend"></div>',
                    '<div class="chart-indicator-form" style="display: none;"></div>',
                    '<div class="chart-indicator-filter-form" style="display: none"></div>',
                    '<div class="chart-series-filters"></div>',
                '</div>',
            '</div>'
            /*eslint-enable */
        ].join('')),
        indicatorFormTmpl: function () {
            return tmpl([
                /*eslint-disable */
                '{% var k, i, length, params, paramItem, indicators = o.indicators; %}',
                '<h5 class="text-center">添加指标</h5>',
                '<form>',
                    '<div class="form-group">',
                        '<label>指标类型</label>',
                        '<select class="form-control" name="type">',
                            '<option value="">请选择</option>',
                            '{% for(k in indicators) {',
                                'if (indicators.hasOwnProperty(k)) { ',
                            '%}',
                                '<option value="{%= k %}">{%= indicators[k].name %}</option>',
                            '{% }',
                                '}',
                            '%}',
                        '</select>',
                    '</div>',
                    '<div class="chart-indicator-params">',
                        '{% for(k in indicators) {',
                            'if (indicators.hasOwnProperty(k)) { ',
                        '%}',
                            '<div class="_for-{%= k %}" >',
                                '{% params = indicators[k].params; %}',
                                '{% for(i=0, length = params.length; i < length; i++) { %}',
                                    '{% paramItem = params[i]; %}',
                                    '<div class="form-group">',
                                        '<label>{%=paramItem.title%}</label>',
                                        '<input type="text" class="form-control" name="{%=paramItem.name%}">',
                                    '</div>',
                                '{% } %}',
                            '</div>',
                        '{% }',
                          '}',
                        '%}',
                    '</div>',
                    '<button type="submit" class="btn btn-primary btn-block">确定</button>',
                    '<button type="button" class="btn btn-default btn-block toggle-indicator-form">取消</button>',
                '</form>'
                /*eslint-enable */
            ].join(''), {
                indicators: this.availableIndicators
            });
        },

        indicatorFilterFormTmpl: function () {
            return tmpl([
                /*eslint-disable */
                '<h5 class="text-center">指标过滤</h5>',
                '<form>',
                    '<div class="form-group">',
                        '<label>指标实例</label>',
                        '<select class="form-control" name="filterIndicator">',
                            '<option value="">请选择</option>',
                        '</select>',
                    '</div>',
                    '<div class="chart-indicator-filter-items"></div>',
                    '<button type="submit" class="btn btn-primary btn-block">确定</button>',
                    '<button type="button" class="btn btn-default btn-block toggle-indicator-filter-form">取消</button>',
                '</form>'
                /*eslint-enable */
            ].join(''));
        },
        indicatorFilterInstanceTmpl: function (indicator) {
            return tmpl([
                '<option value="{%= o.indicator.id %}">{%= o.indicator.name %}</option>'
            ].join(''), {
                indicator: indicator
            });
        },
        indicatorFilterDimensionTmpl: function (indicator) {
            return tmpl([
                /*eslint-disable */
                '{% var $= o.$, indicator = o.indicator, i, ilength, values, j, jLength; %}',
                '{% var filterTitle, filterName, values = null; %}',
                '{% var filters = indicator.filters || {}, filterItems; %}',
                '{% var dims = indicator.info.dimensions; %}',
                '<div class="{%=o.indicator.id%}" data-indicator-id="{%=o.indicator.id%}">',
                    '{% for(i=0, ilength = dims.length; i < ilength; i++) { %}',
                        '{% filterTitle = dims[i].title, filterName = dims[i].name; %}',
                        '<div class="form-group {%=(filters[filterName] && filters[filterName].enabled) ? "active" : "" %}" data-name="{%= filterName %}">',
                            '<label>{%= filterTitle %}</label>',
                            '<select class="form-control dimension-switch">',
                                '<option value="0">不使用过滤</option>',
                                '<option value="1" {%=(filters[filterName] && filters[filterName].enabled) ? "selected" : "" %} >使用过滤</option>',
                            '</select>',
                            '<div class="filter-values">',
                                '{% values = dims[i].values; %}',
                                '{% filterItems = (filters[filterName] && filters[filterName].values)  || [];%}',
                                '{% for(j=0, jLength = values.length; j < jLength; j++) { %}',
                                    '<label class="checkbox-inline">',
                                        '<input type="checkbox" name="{%= filterName %}" {%=($.inArray(values[j], filterItems)>-1 ? "checked" : "")%} value="{%= values[j] %}">{%= values[j] %}',
                                    '</label>',
                                '{% } %}',
                            '</div>',
                        '</div>',
                    '{% } %}',
                '</div>'
                /*eslint-enable */
            ].join(''), {
                indicator: indicator,
                $: $
            });
        },

        toggleForm: function ($form) {
            if ($form.css('right') === '0px') {
                $form.animate({
                    right: -$form.data('width')
                });
            } else {
                $form.animate({
                    right: '0px'
                });
            }
            return false;
        },
        toggleIndicator: function (event) {
            var $currentTarget = $(event.currentTarget);
            var id = $currentTarget.data('id');
            this.findIndicator(id).toggle();
            return false;
        },

        addYaxis: function (id) {
            var axisOpts, chartYaxises, left = 0,
                right = 0;
            var yaxises = this.yaxises;
            if (!yaxises[id]) {
                // 坐标轴还没添加在图表上
                axisOpts = Powchart.YAXISES[id];
                chartYaxises = this.chart.yAxis;
                // 看左边坐标轴多还是右边的多
                $.each(chartYaxises, function (i, v) {
                    if (v.opposite) {
                        right++;
                    } else {
                        left++;
                    }
                });
                axisOpts = left > right ? $.extend({}, axisOpts, {
                    opposite: true
                }) : axisOpts;
                yaxises[id] = 1;
                this.chart.addAxis(axisOpts);
            } else {
                // 增加坐标轴使用次数
                yaxises[id]++;
            }
        },

        removeYaxis: function (id) {
            var yaxises = this.yaxises;
            yaxises[id]--;
            if (yaxises[id] <= 0) {
                this.chart.get(id).remove();
            }
        },

        doAddIndicator: function (data) {
            var info = this.availableIndicators[data.type];
            if (!info) {
                throw new Error('indicator type not available');
            }
            var indicator = new Indicator({
                info: info,
                chart: this.chart,
                params: data.params,
                filters: data.filters || [],
                useSubChart: this.useSubChart
            });
            this.addYaxis(info.yAxisId);
            this.pause = true;
            indicator.init({
                start: this.chart.xAxis[0].dataMin || this.start.valueOf(),
                end: this.end.valueOf(),
                unit: this.unit
            }, $.proxy(function () {
                this.pause = false;
                this.indicators.push(indicator);
                this.addIndicatorDimensions(indicator);
            }, this));
        },

        addIndicator: function (event) {
            event.preventDefault();
            // 获取表单数据
            // TODO 验证表单
            // 添加指标
            var $indicatorForm = this.$indicatorForm;
            this.toggleForm($indicatorForm);
            var $indicatorParams = $indicatorForm.find('.chart-indicator-params > .active');
            var type = $indicatorForm.find('select[name="type"]').val();
            var params = {}, $this, val, error = false;
            $indicatorParams.find('input[type="text"]').each(function () {
                $this = $(this);
                val = $.trim($this.val());
                if (val) {
                    params[$this.attr('name')] = $this.val();
                } else {
                    error = true;
                    return;
                }
            });

            if (!error) {
                this.doAddIndicator({
                    type: type,
                    params: params
                });
                $indicatorForm.find('input[type="text"]').val('');
            }
        },

        _getIndicatorAndPosition: function (id) {
            var index, indicator;
            $.each(this.indicators, function (i, v) {
                if (v.id === id) {
                    index = i;
                    indicator = v;
                    return;
                }
            });
            return {
                index: index,
                indicator: indicator
            };
        },
        findIndicator: function (id) {
            return this._getIndicatorAndPosition(id).indicator;
        },
        removeIndicator: function (event) {
            var info = this._getIndicatorAndPosition($(event.currentTarget).parent().data('id'));
            var indicator = info.indicator;
            var index = info.index;
            if (indicator) {
                // remove from cached indicators
                this.indicators.splice(index, 1);
                // remove from filter form
                this.$indicatorFilterForm.find('.' + indicator.id).remove();
                this.$indicatorFilterForm.find('option[value="' + indicator.id + '"]').remove();
                // remove series
                indicator.destroy();
                // remove yaxis
                this.removeYaxis(indicator.info.yAxisId);
            }
            return false;
        },

        addIndicatorDimensions: function (indicator) {
            var $dimensions = $(this.indicatorFilterDimensionTmpl(indicator));
            this.$indicatorFilterForm
                .find('.chart-indicator-filter-items')
                .append($dimensions);
            this.$indicatorFilterForm
                .find('select[name="filterIndicator"]')
                .append(this.indicatorFilterInstanceTmpl(indicator));
        },

        showIndicatorDimensions: function (event) {
            var $target = $(event.currentTarget);
            var id = $target.val();
            this.$indicatorFilterForm
                .find('.chart-indicator-filter-items>.active')
                .removeClass('active');
            if (id) {
                this.$indicatorFilterForm
                    .find('.' + id)
                    .addClass('active');
            }
        },
        setIndicatorFilter: function (event) {
            // filters = {
            //   'platform': {
            //      enabled: true,
            //      values: ['ios']
            //   },
            //   'qos': {
            //      enabled: false,
            //      values: ['0']
            //   }
            // }
            event.preventDefault();

            var $indicatorFilterForm = this.$indicatorFilterForm;
            this.toggleForm($indicatorFilterForm);

            var $filter = $indicatorFilterForm.find('.chart-indicator-filter-items>.active');
            var id = $filter.data('indicator-id');

            if (!id) {
                return;
            }
            var indicator = this.findIndicator(id);
            var filters = {}, filter, $this;
            $filter.find('.form-group').each(function () {
                $this = $(this);
                filter = {
                    enabled: $this.hasClass('active'),
                    values: $this.find('input[type="checkbox"]:checked').map(function () {
                        return $(this).val();
                    }).get()
                };
                filters[$this.data('name')] = filter;
            });
            this.pause = true;
            indicator.setFilters(filters);
            indicator.update({
                start: this.chart.xAxis[0].dataMin || this.start.valueOf(),
                end: this.end.valueOf(),
                unit: this.unit,
                refresh: true,
                callback: $.proxy(function () {
                    this.pause = false;
                }, this)
            });
        },

        toggleDimension: function (event) {
            var $target = $(event.currentTarget);
            var val = $target.val();
            if (val === '1') {
                $target.parent().addClass('active');
            } else {
                $target.parent().removeClass('active');
            }
        },

        mouseOutIndicator: function (event) {
            this.findIndicator($(event.currentTarget).data('id')).onMouseOut();
        },
        mouseOverIndicator: function (event) {
            this.findIndicator($(event.currentTarget).data('id')).onMouseOver();
        },

        destroy: function () {
            var indicators = this.indicators;
            this.pause = true;
            this.clearInterval();
            $.each(indicators, function (idx, val) {
                val.chart = null;
                val.destroy();
            });
            this.indicators.length = 0;
            this.chart.destroy();
            this.chart = null;
            this.$el.empty();
        }

    };

    Powchart.YAXISES = {};
    // 内置坐标
    Powchart.YAXISES.percentage = {
        id: 'percentage',
        labels: {
            format: '{value}%'
        },
        title: {
            text: null
        },
        max: 100,
        min: 0,
        alignTicks: false
    };

    Powchart.YAXISES.count = {
        id: 'count',
        labels: {
            format: '{value}次'
        },
        title: {
            text: null
        },
        min: 0,
        alignTicks: false
    };

    Powchart.UNITS = [
        {
            title: '10秒',
            value: '10s',
            milliseconds: 10000
        },
        {
            title: '1分钟',
            value: '1m',
            milliseconds: 60000
        },
        {
            title: '5分钟',
            value: '5m',
            milliseconds: 300000
        },
        {
            title: '15分钟',
            value: '15m',
            milliseconds: 900000
        },
        {
            title: '30分钟',
            value: '30m',
            milliseconds: 1800000
        },
        {
            title: '1小时',
            value: '1h',
            milliseconds: 3600000
        },
        {
            title: '4小时',
            value: '4h',
            milliseconds: 14400000
        },
        {
            title: '1天',
            value: '1d',
            milliseconds: 86400000
        },
        {
            title: '7天',
            value: '7d',
            milliseconds: 604800000
        },
        {
            title: '30天',
            value: '30d',
            milliseconds: 18144000000
        }
    ];

    Powchart.UNITS_OF_RANGE = [
        {
            // 30m
            min: 0,
            max: 1800100,
            units: ['1m', '10s', '5m', '15m']
        }, {
            // <1h
            min: 1800100,
            max: 3600100,
            units: ['1m', '5m', '15m', '30m']
        }, {
            // <4h
            min: 3600100,
            max: 14400100,
            units: ['5m', '15m', '30m', '1h']
        }, {
            // <1d
            min: 14400100,
            max: 86400100,
            units: ['15m', '30m', '1h', '4h']
        }, {
            // <7d
            min: 86400100,
            max: 604800100,
            units: ['1h', '4h', '1d']
        }, {
            // <30d
            min: 604800100,
            max: 18144000100,
            units: ['4h', '1d', '7d']
        }, {
            min: 18144000100,
            max: Infinity,
            units: ['1d', '7d', '30d']
        }];

    Highcharts.setOptions({
        colors: ['#058DC7', '#ED561B', '#9B59B6', '#34495E',
            '#DDDF00', '#24CBE5', '#64E572', '#FF9655',
            '#50B432', '#FFF263', '#6AF9C4', '#1ABC9C'],
        global: {
            useUTC: false
        }
    });

    return Powchart;
}));

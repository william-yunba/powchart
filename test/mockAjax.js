// simple mock ajax for testing
(function() {
    $.originAjax = $.ajax;
    $.ajax = function(options) {
        var pathname = options.url;
        var start = options.data.start;
        var end = options.data.end;
        var unit = options.data.unit;
        var step = getStep(unit);
        var success = options.success;
        var time, data;
        switch(true) {
            case null !== pathname.match(/\/stat\/mechine\/[^\/]+\/cpu-utilization/)
                || null !== pathname.match(/\/stat\/mechine\/[^\/]+\/mem-utilization/):
                time = start, data = [];
                for (time = parse(time, step); time < end; time += step) {
                    data.push([time, cpuPoint()]);
                }
                success(data, 'success');
                break;
            case null !== pathname.match(/\/stat\/app\/[^\/]+\/publish/)
                || null !== pathname.match(/\/stat\/app\/[^\/]+\/subscribe/):
                time = start, data = [];
                for (time = parse(time, step); time < end; time += step) {
                    data.push([time, cpuPoint()]);
                }
                success(data, 'success');
                break;
            default:
                $.originAjax.apply(null, arguments);
                break;
        }
    }

    function publishPoint() {
        return parseInt(Math.random() * 8000);
    }

    function cpuPoint() {
        var val = Math.random();
        // 控制概率 
        if ( ((val < 0.5 || val > 0.6) && Math.random() < 0.95 ) || val < 0.3 || val > 0.9) {
            return cpuPoint();
        } else {
            return Number((val * 100).toFixed(2))
        }
    }

    function getStep(type) {
        switch(type) {
            case '10s':
                return 10000;
            case '1m':
                return 60000;
            case '5m':
                return 300000;
            case '15m':
                return 900000;
            case '30m':
                return 1800000;
            case '1h':
                return 3600000;
            case '4h':
                return 14400000;
            case '1d':
                return 86400000;
            case '7d':
                return 604800000;
            case '30d':
                return 18144000000;
            default:
                return Infinity;
        }
    }

    function parse(time, step) {
        return Math.ceil((time/step)) * step;
    }

})();
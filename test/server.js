var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var querystring = require('querystring');
var server = http.createServer(function(req, res) {
    var parsedUrl = url.parse(req.url);
    var pathname = parsedUrl.pathname;
    var query = querystring.parse(parsedUrl.query);

    // file
    var match = pathname.match(/\.(js|css|html)$/i)
    if (match) {
        if (match[1] == 'css') {
            res.writeHead(200, {'Content-Type': 'text/css' });
        } else if (match[1] == 'js') {
            res.writeHead(200, {'Content-Type': 'application/javascript' });
        }
        fs.createReadStream(__dirname + '/..' + pathname).pipe(res);
        return;
    }

    res.writeHead(200, {'Content-Type': 'application/json' });
    // api
    var start = Number(query.start);
    var end = Number(query.end);
    var step = getStep(query.unit);
    switch(true) {
        case pathname == '/favicon.ico':
            res.end();
            break;
        case null !== pathname.match(/\/stat\/mechine\/[^\/]+\/cpu-utilization/):
            var time = start, data = [];
            for (time += step ; time <= end; time += step) {
                data.push([time, cpuPoint()]);
            }
            res.end(JSON.stringify(data));
            break;
        case null !== pathname.match(/\/stat\/app\/[^\/]+\/publish/):
            var time = start, data = [];
            for (time += step ; time <= end; time += step) {
                data.push([time, publishPoint()]);
            }
            res.end(JSON.stringify(data));
        default:
            res.end('[]');
            break;
    }
});

function publishPoint() {
    return parseInt(Math.random() * 8000);
}

function cpuPoint() {
    var val = Math.random();
    // 控制概率
    if ( (val < 0.3 ) || (val > 0.8 && Math.random() < 0.9) ) {
        return cpuPoint();
    } else {
        return Number((val * 100).toFixed(2))
    }
}

function getStep(type) {
    switch(type) {
        case '10s':
            return 10000;
        case '1m':
            return 60000;
        case '5m':
            return 300000;
        case '15m':
            return 900000;
        case '1h':
            return 3600000;
        case '4h':
            return 14400000;
        case '1d':
            return 86400000;
        case '7d':
            return 604800000;
        case '30d':
            return 18144000000;
        default:
            return Infinity;
    }
}

server.listen('8081');
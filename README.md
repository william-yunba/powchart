Powchart.js
==
一个基于`HighCharts`的图表UI组件， 主要用于其中一维是时间的二维数据展示， 图表监控。  
## 演示
[DEMO](http://william-yunba.bitbucket.org/powchart/demo.html)
## 功能  
 * 自定义图表指标
 * 一个图表能附加多个指标  
 * 每个指标可以筛选维度  
 * 能自定义时间段和时间间隔
 * 自动根据间隔动态更新指标  

## 使用  
#### 1. 引入依赖和组件
```html
<!-- jquery -->
<script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.js"></script>
<!-- bootstrap 3 -->
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- bootstrap daterangepicker -->
<script  src="http://cdn.bootcss.com/bootstrap-daterangepicker/2.1.18/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/bootstrap-daterangepicker/2.1.18/daterangepicker.min.css" />
<!-- highcharts -->
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<!-- moment.js -->
<script src="http://cdn.bootcss.com/moment.js/2.11.2/moment.min.js"></script>

<!-- powchart -->
<script src="powchart.js"></script>
<script src="powchart.css"></script>
```

#### 2. 自定义指标类型和实例化图表
```js
// 你需要先定义用在图表中的指标类型
// 例如下面定义一个publish指标
var publish = {
    // 指标名称
    name: '发送次数',
    // 请求数据地址, :开头为参数
    url: '/stat/app/:appkey/publish',
    // url中的参数
    params: [{
        // 参数显示名
        title: 'APPKEY',
        // 参数在url中的替换名
        name: 'appkey'
    }],
    // 指标可以筛选的维度
    dimensions: [
        {
            // 维度名称
            name: 'platform',
            // 维度选项
            values: ['ios', 'android']
        },
        {
            name: 'qos',
            values: ['0', '1', '2']
        }
    ]
    // 该指标使用的纵坐标类型， 目前支持两种， 分别为 count 和 percentage
    // 详情看下面
    yAxisId:'count',
}

// 实例化图表
new Powchart({
    // 图表容器 dom object/jquery object/selector
    el: '.my-chart'，
    // 图表监控时间范围的开始时间 milliseconds/datetime str/moment object
    start: moment().subtract(1, 'hours'),
    // 图表监控时间范围的结束时间
    end: moment(),
    // 点间隔 5分钟，其它见下面
    unit: '5m',
    // 是否使用副图表， 目前只能用于percentage坐标的指标
    useSubChart: false,
    // 这个图表中用户能添加的指标类型, 你可以通过这个限制这个图表只能添加特定类型的指标
    // 例如下面这样设定， 这个图表只能添加 publish 这种类型的指标
    availableIndicators: {
      publish: publish
    }
});
```
#### 3. 完成. 这时候用户就可以在图表上随意添加指标了

## 接口  
`new Powchart(options:Object)` 
 
 * options: Object  
    必须  
    图表参数  

 * options.el: Dom Object / Dom selector / jQuery Object  
    必须  
    渲染图表的容器  

 * options.start: time str / time milliseconds / moment time Object  
    可选  
    图表开始时间，默认 _（当前时间-30分钟）_  

 * options.end: time str / time milliseconds / moment time Object  
    可选  
    图表结束时间，默认 _当前时间_  

 * options.unit: '10s' | '1m' | '5m' | '15m' | '30m' | '1h' | '4h' | '1d'   
    可选  
    图表上点的间隔，默认 _10s_  
    需要注意的是， 考虑到数据点的数量， 对于一个特定的开始时间至结束时间范围，都只有部分时间间隔可用。  

 * options.useSubChart: Boolean  
    可选  
    是否启用副图表， 当前只能用于纵坐标为百分比的指标。默认`false`  

 * options.availableIndicators: Object  
    必须  
    这个图表可添加哪些指标。每个指标应包含`name`,`url`,`yAxisId`等字段，见下面例子：  

```js
    {
        // 指标key
        publish: {
          // 指标名称
          name: '发送次数',
          // 指标数据地址
          url: '/stat/app/:appkey/publish',
          // 纵坐标类型，percentage | conut
          yAxisId:'count',
          // url中的参数
          params: [{
              title: 'APPKEY',
              name: 'appkey'
          }],
          // 可筛选过滤维度
          dimensions: [
              {
                  // 维度名称
                  name: 'platform',
                  // 可供过滤的选项
                  values: ['ios', 'android']
              },
              {
                  name: 'qos',
                  values: ['0', '1', '2']
              }
          ]
        }
    }
```
* options.initIndicators: Array  
    可选  
    在图表实例化的时候自动添加这些指标。例子  
```js
    [
        {
            // 指标类型
            type: 'publish',
            // 指标数据url参数
            params: {
              appkey: 'app1'
            },
            // 维度过滤
            filters: {
              platform: {
                // 启用这个过滤项
                enabled: true,
                // 被选中的过滤项，可多个
                values: ['android']
              }
            }
        }
    ]
```

## 指标定义和服务端数据接口  
#### 指标定义  
用一个Object定义一个指标， 应包括以下属性  

 * name: String  
  必须  
  指标名称
 * url: String  
  必须  
  用于请求指标数据的URL，可以使用`:paramname`的形式标记url中的参数， 将会被实力中的参数值替换
 * params: Array  
  可选  
  指明url中的参数列表。每个参数对象应包含`title`（给用户看）和`name`（在url中的替换名）属性。例子  

```js
[{
  title: 'APPKEY',
  name: 'appkey'
}]
``` 
* dimensions: Array  
  可选  
  该指标可以供筛选的维度和选项。每个维度应包含`title`，`name`(传给服务端的时候用)和`values`属性。例子  
```js  
[
  {
      title: '平台'，
      name: 'platform',
      values: ['ios', 'android']
  },
  {
      title: '发送质量',
      name: 'qos',
      values: ['0', '1', '2']
  }
]

```
* yAxisId: String  
  必须  
  指定指标所使用的纵坐标类型。现在内置2种，分别为`percentage`和`count`。如果你需要扩展，你可以在`Powchart.YAXISES`里添加你的自定义纵坐标。参照下面  
```js  
Powchart.YAXISES.percentage = {
    id: 'percentage',
    labels: {
        format: '{value}%'
    },
    title: {
        text: null
    },
    max: 100,
    min: 0,
    alignTicks: false
}

Powchart.YAXISES.count ={
    id: 'count',
    labels: {
        format: '{value}次'
    },
    title: {
        text: null
    },
    min: 0,
    alignTicks: false
}

```

#### 服务端数据接口  
URL： 指标定义的URL  
方法： GET  
参数：  

 * start: Number  
  必须  
  精确到毫秒的时间戳。图表数据的开始时间  
 * end: Number  
  必须  
  精确到毫秒的时间戳。图表数据的结束时间  
 * unit: String  
    必须  
    数据点的时间间隔。默认有 `'10s'`， `'1m'`， `'5m'`， `'15m'`， `'30m'`， `'1h'`， `'4h'`和`'1d'`。可与前端约定  
  * filters: String  
    可选  
    过滤项。JSON字符串，需要服务端parse成JSON对象读取。例子 <br />
              `{"platform":["ios"],"qos":["1","2"]}`

#### 其它
URL例子  
```text
/stat/mechine/:id/cpu-utilization
/stat/mechine/:id/memory-utilization
/stat/mechine/:id/internet-out-rate
/stat/mechine/:id/internet-in-rate
/stat/mechine/:id/internet-out
/stat/mechine/:id/internet-in
/stat/mechine/:id/tcp

/stat/app/:appkey/publish
/stat/app/:appkey/subscribe
/stat/app/:appkey/unsubscribe
/stat/app/:appkey/online-user
/stat/app/:appkey/active-user

/stat/app/:appkey/topic/:topic/publish
/stat/app/:appkey/topic/:topic/subscribe
/stat/app/:appkey/topic/:topic/unsubscribe

```

## Change Log
